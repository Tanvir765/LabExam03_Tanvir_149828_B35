<?php
$temperatures = array(78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74, 62, 62, 65, 64, 68, 73, 75, 79, 73);

function printAverage($array)
{
    $total = 0;
    foreach($array as $element)
    {
        $total += $element;
    }
    echo number_format($total / count($array));
}

echo "<br>";

echo "Average Temperature is : ";
printAverage($temperatures);
echo "<br>";

sort($temperatures);

echo "List of five lowest temperatures : ";
echo "[";
for($i = 0; $i < 5; $i++)
{
    echo "$temperatures[$i]";
    if($i==4){
        continue;
    }
    echo ",";

}
echo "]";
echo "<br>";

echo "List of five highest temperatures : ";

echo "[";
for($i = count($temperatures) - 5; $i <= count($temperatures) - 1; $i++)
{
    echo "$temperatures[$i]";
    echo ",";
}
echo "]";

?>